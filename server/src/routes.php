<?php

//bd conexion
require_once 'models/postgresDB.php';
require_once 'models/controller.php';
// Routes

$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    $data = array('name' => 'Rob', 'age' => 40);
    return $response->withJson($data, 200);
});


$app->post('/login/', function ($request, $response, $args) {
	
	$data = $request->getParsedBody();
	$email = $data["email"];
	$pass = md5($data["pass"]);
	$db =  new db();
	$users = $db->sql("select count(id) from usuarios where email = '$email' and pass = '$pass';");
	
	if($users[0]["count"]>0)
	{
		$token = $token = bin2hex(openssl_random_pseudo_bytes(16));
		$db->sql("update usuarios set token = '$token' where email = '$email';");
		$resp = Array('token'=>$token);
	}
	else{
		$resp = Array('error'=>'El usuario no existe');
	}
	return $response->withJson($resp, 200);
});

$app->post('/register/', function ($request, $response, $args) {
	
	$data = $request->getParsedBody();
	$db =  new db();
	
	if(!is_null($data) && !empty($data))
	{
		$email = $data["email"];
		$users = $db->sql("select count(id) from usuarios where email = '$email'");
		$this->logger->info($email." - ".$users[0]["count"]);
		if($users[0]["count"] == 0)
		{
			$data["pass"] = md5($data["pass"]);
			$db->insert($data, "usuarios");
			$token = $token = bin2hex(openssl_random_pseudo_bytes(16));
			$db->sql("update usuarios set token = '$token' where email = '$email';");
			$resp = Array('token'=>$token);
		}
		else{
			$resp = array('error' => 'El email ya existe');
		}
	}
	else{
		$resp = array('result' => 'false');
	}
	
	return $response->withJson($resp, 200);
});

$app->post('/obtener/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
		$tabla = $data["tabla"];
		$proyectos = $db->sql("select *  from $tabla order by id limit 100");
		$resp = $proyectos;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});	

$app->post('/buscar/', function ($request, $response, $args) {
	
	$data = $request->getParsedBody();
	$tabla = $data["tabla"];
	$token = $data["token"];
	unset($data["tabla"]);
	unset($data["token"]);
	$keys = array_keys($data);
	$db =  new db();
	$sql = "select *  from $tabla where";
$campos = count($keys);
$i = 0;
$this->logger->info($campos);
	if($db->checkToken($token)){
			foreach($keys as $key)
			{
				if($i>0 && $i < $campos) {
					$sql .= " AND  ";
				}

				if(($key == "id" || str_split($key,3)[0]=="cod" || $key == "activo") && $key !="codcliente")
					{
						$val = $data[$key];
						$sql .= (" $key = '$val' ");
						}
				else
				{
					$val = $data[$key];
					$sql .= (" $key LIKE '%$val%'");
				}
				$i++;
			}
			$resp =  $db->sql($sql);
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
		$this->logger->info($sql);
	return $response->withJson($resp, 200);
});

$app->post('/insertar/', function ($request, $response, $args) {
	
	$data = $request->getParsedBody();
	$db =  new db();
	$this->logger->info("******** Insertar **********");
	$this->logger->info($data);
	foreach($data as $item){
		$this->logger->info($item);
	}
	
	if(!is_null($data) && !empty($data))
	{
		if($db->checkToken($data["token"])){
			$result = $db->insert($data, $data["tabla"]);
//$this->logger->info("Insertar".$result);
			$resp = $result;
		}
		else{
			$resp = array('error' => 'Token no valido');
		}
	}
	else{
		$resp = array('result' => 'false');
	}
	
	return $response->withJson($resp, 200);
});
$app->post('/actualizar/', function ($request, $response, $args) {
	
	$data = $request->getParsedBody();
	$db =  new db();$this->logger->info("Actualizar ".$data["id"]." ".$data["token"]." ".$data["tabla"]);
	if(!is_null($data) && !empty($data))
	{
		if($db->checkToken($data["token"])){
			$result = $db->update($data, $data["tabla"],$data["id"]);
			$resp = $result;
		}
		else{
			$resp = array('error' => 'Token no valido');
		}
	}
	else{
		$resp = array('result' => 'false');
	}
	
	return $response->withJson($resp, 200);
});
$app->post('/eliminar/', function ($request, $response, $args) {
	
	$data = $request->getParsedBody();
	$db =  new db();
	
	if(!is_null($data) && !empty($data))
	{
		if($db->checkToken($data["token"])){
			$result = $db->delete($data, $data["tabla"]);
			$resp = $data;
		}
		else{
			$resp = array('error' => 'Token no valido');
		}
	}
	else{
		$resp = array('result' => 'false');
	}
	
	return $response->withJson($resp, 200);
});
$app->post('/actualizarRol/', function ($request, $response, $args) {
	
	$data = $request->getParsedBody();
	$db =  new db();
	
	if(!is_null($data) && !empty($data))
	{
		if($db->checkToken($data["token"])){
			$tabla = $data['tabla'];
			$equipo = $data['equipo'];
			$usuario = $data['usuario'];
			$rol = $data['rol'];
			$this->logger->info("UPDATE $tabla rol='$rol' where equipo=$equipo and usuario = $usuario");
			$resultados = $db->sql("UPDATE $tabla SET rol='$rol' where equipo=$equipo and usuario = $usuario");
			$resp = $resultados;
		}
		else{
			$resp = array('error' => 'Token no valido');
		}
	}
	else{
		$resp = array('result' => 'false');
	}
	
	return $response->withJson($resp, 200);
});
		
$app->post('/addIcono/', function ($request, $response, $args) {
	$data = $request->getParsedBody();
	$db =  new db();
	$folder = "public/images/icons/";
	$url = "http://".$_SERVER['HTTP_HOST']."/images/icons/";
	$imgs = scandir($folder,1);
	
	if(count($imgs)>0)
		$id = str_replace(".jpg","",$imgs[0])+1;
	else
		$id = 0;

	$img = $data["file"]; 
	$img = str_replace('data:image/jpeg;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$str = base64_decode($img);
	file_put_contents($folder.$id.'.jpg', $str);
	$resp = array('icono' => $url.$id.'.jpg');
	$url =  $url.$id.'.jpg';
	$tabla = $data["tabla"];
	$id = $data["id"];
		$result = $db->sql("update $tabla set icono = '$url'  WHERE id = '$id' ");
		$resp = $result;
  $this->logger->info("update $tabla set icono = '$url'  WHERE id = '$id' ");
	
return $response->withJson($resp, 200);
});
$app->post('/obtenerUsuario/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
		$token = $data['token'];
		$usuario = $db->sql("select *  from usuarios  WHERE token = '$token'");
		$resp = $usuario;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});	
$app->post('/obtenerUsuarios/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
		$equipo = $data['equipo'];
		$this->logger->info("equipos: ".$equipo);
		$usuarios = $db->sql("select *  from usuarios INNER JOIN equipos_usuarios ON usuarios.id = equipos_usuarios.usuario WHERE equipos_usuarios.equipo = $equipo");
		$resp = $usuarios;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});	
$app->post('/obtenerEquipos/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
		$token = $data['token'];
		$equipos = $db->sql("select *  from equipos INNER JOIN equipos_usuarios ON equipos.id = equipos_usuarios.equipo WHERE equipos_usuarios.usuario in (select id from usuarios where token ='$token')");
		$resp = $equipos;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});
$app->post('/obtenerTareas/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
		$equipo = $data["team"];
			$estado = $data["state"];
			$tabla = $data["table"];
		if($tabla == "tareas")
		{
			$this->logger->info("select h.nombre as nombreh, h.id as historiaid, usuarios.id as idusuario, *  from $tabla as h left join usuarios on h.codresponsable = usuarios.id  WHERE equipo = $equipo and estado = $estado");
			$tareas = $db->sql("select h.nombre as nombret, h.id as codtarea, usuarios.id as idusuario, *  from $tabla as h left join usuarios on h.codresponsable = usuarios.id  WHERE equipo = $equipo and estado = $estado order by orden ASC");
		}
		else{
			$tareas = $db->sql("select  *  from $tabla   WHERE equipo = $equipo and estado = $estado order by orden ASC");
		}
		$resp = $tareas;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});	
$app->post('/obtenerComentarios/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
		if(!empty($data['historia']) && $data['historia']=!"")
		{
			$historia = $data['historia'];
			$comentarios = $db->sql("select *, usuarios.id as idusuario  from comentarios LEFT JOIN usuarios ON usuarios.id = comentarios.usuario WHERE comentarios.historia = $historia order by comentarios.fecha DESC");
		}
		else{
			$tarea = $data['tarea'];
			$comentarios = $db->sql("select *, usuarios.id as idusuario  from comentarios LEFT JOIN usuarios ON usuarios.id = comentarios.usuario WHERE comentarios.tarea = $tarea order by comentarios.fecha DESC");
		}
				$resp = $comentarios;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});	
$app->post('/obtenerSprintBurndown/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
		$equipo = $data['equipo'];
		$fechainicio = $data['fechainicio'];
		$fechafin = $data["fechafin"];
		$this->logger->info("******** Sprint *****");
		$this->logger->info($fechainicio." - ".$fechafin." equipo: ".$equipo);
		$esfuerzo = $db->sql("select esfuerzoestimado from sprints where codequipo = $equipo and activo = true;");
		//$this->logger->info("select fecha,sum(horasestimadas) from tareas where fecha > '$fechainicio' and fecha < '$fechafin' and equipo = $equipo group by fecha");
		$tareasCompletadas = $db->sql("select fecha,sum(horasestimadas)  from tareas where fecha >= '$fechainicio' and fecha <= '$fechafin' and equipo = 1 group by fecha");
		$fecha= new DateTime($fechainicio);
		$fechafin = new DateTime($fechafin);
		$sprint = array(array("Horas", "Esfuerzo restantes","Ideal"));
	//	array_push($sprint, array("Horas", "Esfuerzo restantes","Ideal"));
		$esfuerzoTotal = $esfuerzo[0]["esfuerzoestimado"];
		$esfuerzoTotal2 = $esfuerzo[0]["esfuerzoestimado"];
		array_push($sprint, array("",$esfuerzoTotal,$esfuerzoTotal ));
		$interval = $fecha->diff($fechafin);
		
		$ideal = $esfuerzoTotal/intval($interval->format('%a'));
		

		while($fecha < $fechafin){
				$esfuerzo =  getEffort($tareasCompletadas,$fecha->format('Y-m-d'));
				if($esfuerzo != false){
					$esfuerzoTotal -= $esfuerzo;
				}
				$esfuerzoTotal2 -= $ideal;
				array_push($sprint, array($fecha->format('d'),$esfuerzoTotal,$esfuerzoTotal2 ));
			
				$fecha->add(new DateInterval('P1D'));
		}
		
		$resp = $sprint;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});
$app->post('/obtenerVelocidad/', function ($request, $response, $args) {

	$data = $request->getParsedBody();
	$db =  new db();
	
	if($db->checkToken($data["token"])){
			$equipo = $data['codequipo'];
			$velocidad = $db->sql("select sum(esfuerzocompletado) as completados, sum(horasdisponibles) as horas from sprints where codequipo = $equipo");
			$resp = $velocidad;
	}
	else{
		$resp = array('error' => 'Token no valido');
	}
	return $response->withJson($resp, 200);
});	
 function getEffort($tareas,$fecha){
		foreach($tareas as $tarea)
		{
			if($tarea["fecha"]==$fecha)
			return $tarea["sum"];
		}
		return false;
}