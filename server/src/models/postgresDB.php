<?php 
class db{
	var	$pdo;
	
	function db(){
		
		$dsn = 'pgsql:host=localhost;dbname=scrumtool';
		$usr = 'ivan';
		$pwd = '123456';
		
		$this->pdo = new \Slim\PDO\Database($dsn, $usr, $pwd);
		
		// SELECT * FROM users WHERE id = ?
	/*	$selectStatement = $pdo->select()
		->from('proyectos');
		
		$stmt = $selectStatement->execute();
		$data = $stmt->fetchAll();
	*/

	}
public function insert($data,$table){
	
	//Limpio campos de data que no son necesarios
	unset($data["token"]);
	unset($data["tabla"]);
	//Inserto
	$insertStatement = $this->pdo->insert(array_keys($data))
	->into($table)
	->values(array_values($data));
	return $insertStatement->execute(false);
}
public function update($data,$tabla,$id){
	//Limpio campos de data que no son necesarios
	unset($data["token"]);
	unset($data["tabla"]);
	unset($data["id"]);
	$tipos = $this->getTypes($tabla);
	$sql = "UPDATE $tabla SET ";
	$campos = count(array_keys($data));
	$i = 0;
	foreach(array_keys($data) as $key ){
	
		if(isset($data[$key]) && $data[$key] !== "")
		{
			//Pongo coma si es necesario
			if($i>0 && $i < $campos) {
        	$sql .= ", ";
			}
			//Pongo comillas en campos necesarios
			if($tipos[$key])
			{
				$sql .= "$key = '$data[$key]'";
			}
			else{
				$sql .= "$key = $data[$key]";
			}
		}
		$i++;
	}
	$sql .= " WHERE id =".intval($id).";";
	//return $sql;
	return $this->sql($sql);
}
public function delete($data,$tabla){
	//Limpio campos de data que no son necesarios
	unset($data["token"]);
	unset($data["tabla"]);
	$sql = "DELETE from $tabla WHERE";
	$campos = count(array_keys($data));
	$i = 0;
	foreach(array_keys($data) as $key ){
		 if( $i < $campos -1) {
        	$sql .= " $key = '$data[$key]' and ";
    	}
		else{
			$sql .= " $key = '$data[$key]' ";
		}
		$i++;
	}
	//return $sql;
	return $this->sql($sql);
}
public function sql($sql){

	$res = $this->pdo->prepare($sql);
	$res->execute();
	return $res->fetchAll(PDO::FETCH_ASSOC);
}

public function checkToken($token){
	
	$valid = false;
	if(!empty($token))
	{
		$res = $this->sql("select count(id) from usuarios where token = '$token'");
		if($res[0]["count"] > 0 )
			$valid = true;
	}
	
	return $valid;
	
}
protected function getTypes($table){
	$tipos ='
		"proyectos":{
			"nombre":"1",
			"descripcion":"1",
			"estado":"1",
			"fechainicio":"1",
			"fechafin":"1",
			"imputable":"0",
			"mantenimiento":"0",
			"presupuesto":"0",
			"facturado":"0",
			"horasestimadas":"0",
			"horasdedicadas":"0",
			"horasfacturadas":"0",
			"desviacionh":"0",
			"rentabilidadh":"0",
			"margenh":"0",
			"costeexterno":"0",
			"costeinterno":"0",
			"costetotal":"0",
			"rentabilidad":"0",
			"margen":"0",
			"codcliente"":"1"
		},
		"clientes":{
			"nombre":"1",
			"apellidos":"1",
			"nombrecomercial":"1",
			"cifnif":"1",
			"email":"1",
			"telefono1":"1",
			"fechaalta":"1",
			"fechabaja":"1"
			},
		"historiasusuario":{
			"orden":"0"
			},
			"tareas":{
			"orden":"0"
			}	
	';
	$tipos = json_encode($tipos);
	return $tipos[$table];
}
}
?>